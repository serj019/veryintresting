//
//  SearchView.swift
//  TestVeryInteresting
//
//  Created by Serj Potapov on 01.11.2022.
//

import UIKit

class SearchView: UIView {
    
    let searchTF = UITextField(placeholder: "Введите запрос...")
    let searchButton = UIButton(text: "Найти",
                                color: .blue,
                                image: nil,
                                buttonType: .rounded)
    let collectionView = UICollectionView(frame: CGRect(), collectionViewLayout: CompositionalLayoutManager.shared.createLayout())
    
    init() {
        super.init(frame: CGRect())
        setViews()
        setConstraints()
    }
    
    func setViews() {
        collectionView.backgroundColor = .white
        collectionView.register(ImageCell.self, forCellWithReuseIdentifier: ImageCell.reuseID)
    }
    
    func setConstraints() {

        
        let searchStack = UIStackView(views: [searchTF,
                                              searchButton],
                                      axis: .horizontal,
                                      spacing: 6)
        
        let stack = UIStackView.init(views: [searchStack,
                                             collectionView],
                                     axis: .vertical,
                                     spacing: 6)
        
        stack.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stack)
        
        NSLayoutConstraint.activate([
            stack.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 6),
            stack.leftAnchor.constraint(equalTo: safeAreaLayoutGuide.leftAnchor, constant: 6),
            stack.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
            stack.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -6),
        ])
        
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}
