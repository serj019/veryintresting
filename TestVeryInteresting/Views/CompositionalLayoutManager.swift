//
//  CompositionalLayoutManager.swift
//  BarnaulStore
//
//  Created by Serj Potapov on 20.10.2022.
//

enum SectionNumber: Int {
    case images = 0
}

import UIKit

class CompositionalLayoutManager {
    
    static let shared = CompositionalLayoutManager(); private init() { }
    
    func createLayout() -> UICollectionViewLayout {
        
        let layout = UICollectionViewCompositionalLayout { section, _ in
            
            self.createImages()
            
        }
        
        return layout
    }
    
    func createImages() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.34),
                                               heightDimension: .fractionalWidth(0.33))
        
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                               heightDimension: .fractionalWidth(0.33))
        
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize,
                                                       repeatingSubitem: item,
                                                       count: 3)
        
        group.interItemSpacing = NSCollectionLayoutSpacing.fixed(2)
        
        let section = NSCollectionLayoutSection(group: group)
        section.interGroupSpacing = 0
//        section.orthogonalScrollingBehavior = .continuous
        
        return section
    }
    
    
}



