//
//  UIButtonExt.swift
//  TestAlef
//
//  Created by Serj Potapov on 26.10.2022.
//

import UIKit

extension UIButton {
    
    convenience init(text: String, color: UIColor, image: UIImage?, buttonType: DesingType) {
        self.init(type: .system)
        
        setTitle(text, for: .normal)
        setImage(image, for: .normal)
        setTitleColor(color, for: .normal)
        tintColor = color
        
        switch buttonType {
        case .rounded:
            
            layer.cornerRadius = 6
            layer.borderColor = color.cgColor
            layer.borderWidth = 2
            clipsToBounds = true
            if let imageView, let titleLabel {
                NSLayoutConstraint.activate([heightAnchor.constraint(equalToConstant: 48),
                                             leftAnchor.constraint(equalTo: imageView.leftAnchor, constant: -12),
                                             rightAnchor.constraint(equalTo: titleLabel.rightAnchor, constant: 12) ])
            }
            
        case .usual:
            backgroundColor = .red
        }
        
    }
    
    
    enum DesingType {
        case rounded
        case usual
    }
    
}
