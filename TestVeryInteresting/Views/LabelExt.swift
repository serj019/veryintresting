//
//  LabelExt.swift
//  TestAlef
//
//  Created by Serj Potapov on 26.10.2022.
//

import UIKit

extension UILabel {
    
    convenience init(text: String, type: LabelType) {
        self.init(frame: CGRect())
        self.text = text
        switch type {
        case .bigBlack:
            self.font = .systemFont(ofSize: 24)
            textColor = .black
        case .smallGray:
            self.font = .systemFont(ofSize: 14)
            textColor = .gray
        }
    }
    
    enum LabelType {
        case bigBlack
        case smallGray
    }
    
}
