//
//  CampaignCell.swift
//  BarnaulStore
//
//  Created by Serj Potapov on 20.10.2022.
//

import UIKit

class ImageCell: UICollectionViewCell {
    static let reuseID = "ImageCell"
    let imageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: CGRect())
        backgroundColor = .white
        setView()
        setConstraints()
    }
    
    func setView() {
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
    }
   
    func setConstraints() {
        addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: topAnchor),
            imageView.leftAnchor.constraint(equalTo: leftAnchor),
            imageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            imageView.centerYAnchor.constraint(equalTo: centerYAnchor)])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

