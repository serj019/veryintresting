//
//  TexfTieldExt.swift
//  TestAlef
//
//  Created by Serj Potapov on 26.10.2022.
//

import UIKit

extension UITextField {
    convenience init(placeholder: String) {
        self.init(frame: CGRect())
        backgroundColor = .white
        font = .systemFont(ofSize: 16)
        translatesAutoresizingMaskIntoConstraints = false
        
        self.placeholder = placeholder
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 6
        self.clipsToBounds = true
        leftView = UIView(frame: CGRect(x: 0, y: 0, width: 4, height: 0))
        leftViewMode = .always
        
        let xButton = UIButton(type: .system)
        xButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        xButton.setImage(UIImage(systemName: "x.circle.fill"), for: .normal)
        xButton.tintColor = .gray
        
        let action = UIAction { _ in
            self.text?.removeAll()
        }
        
        xButton.addAction(action, for: .touchUpInside)
        
        rightView = xButton
        rightViewMode = .whileEditing
    }
}
