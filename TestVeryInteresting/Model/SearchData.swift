//
//  SearchData.swift
//  TestVeryInteresting
//
//  Created by Serj Potapov on 01.11.2022.
//

import Foundation


struct SearchData: Decodable {
    var imagesResults: [ImageResult]
    
    struct ImageResult: Decodable {
        var original: String
        var thumbnail: String
    }
    
}
