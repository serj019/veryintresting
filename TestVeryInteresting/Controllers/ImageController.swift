//
//  ImageController.swift
//  TestVeryInteresting
//
//  Created by Serj Potapov on 01.11.2022.
//

import UIKit

class ImageController: UIViewController {
    
    let mainView = UIImageView()
    var imageUrl: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mainView
        mainView.backgroundColor = .white
        mainView.contentMode = .scaleAspectFill
        downloadImage()
    }

    func downloadImage() {
        
        Task {
            guard let imageData = try await NetworkService.shared.downloadImage(by: imageUrl) else {
                self.dismiss(animated: true)
                return
            }
            
            guard let image = UIImage(data: imageData) else {
                self.dismiss(animated: true)
                return
            }
            
            self.mainView.image = image
        }
        
        
    }

}
