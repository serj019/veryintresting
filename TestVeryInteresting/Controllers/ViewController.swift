//
//  ViewController.swift
//  TestVeryInteresting
//
//  Created by Serj Potapov on 01.11.2022.
//

import UIKit

class ViewController: UIViewController {
    
    let mainView = SearchView()
    
    var pagging = 0
    
    var searchData: SearchData? {
        didSet {
            mainView.collectionView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view = mainView
        
        mainView.collectionView.dataSource = self
        mainView.collectionView.delegate = self
        addSearchAction()
    }
    
    func addSearchAction() {
        let searchAction = UIAction { [weak self] _ in
            guard let searchText = self?.mainView.searchTF.text else { return }
            self?.pagging = 0
            Task {
                do {
                    let searchData = try await NetworkService.shared.getImages(searchText: searchText, page: self!.pagging)
                    self?.searchData = searchData
                    print("COUNT:\(searchData.imagesResults.count)")
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
        
        self.mainView.searchButton.addAction(searchAction, for: .touchUpInside)
        
    }


}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return searchData?.imagesResults.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCell.reuseID, for: indexPath) as! ImageCell
    
        Task {
            do {
                guard let url = searchData?.imagesResults[indexPath.item].thumbnail else {
                    return
                }
                guard let imageData = try await NetworkService.shared.downloadImage(by: url) else { return }
                guard let image = UIImage(data: imageData) else { return }
                cell.imageView.image = image
            }
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        
        let vc = ImageController()
        guard let url = searchData?.imagesResults[indexPath.item].original else {
            return
        }
        vc.imageUrl = url
        self.present(vc, animated: true)
    }

    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        print("scrollViewDidEndDecelerating")
        if mainView.collectionView.contentOffset.y >= mainView.collectionView.contentSize.height - mainView.collectionView.frame.size.height {
            print (#function)
            
            guard let searchText = self.mainView.searchTF.text else { return }

            self.pagging += 1
            
            print("New page: \(self.pagging)")
            Task {
                let addedSearchData = try await NetworkService.shared.getImages(searchText: searchText,
                                                page: self.pagging)
                
                self.searchData?.imagesResults += addedSearchData.imagesResults
            }
            
            
        }
    
    }


}
