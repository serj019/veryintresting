//
//  NetWork .swift
//  BarnaulStore
//
//  Created by Serj Potapov on 17.10.2022.
//

import Foundation
import SwiftyJSON


class NetworkService {
    static let shared = NetworkService()
    private init() {}
    
  //https://serpapi.com/playground?q=Apple&tbm=isch&num=20&device=mobile
    
    private let apiKey = "d666eba989bee3b758030118eff9e5330c2c8d6a1e87ae35ee0fce8fdea20fa2"
    
    //MARK: - создание урла и подключание для первого экрана
    private func createURL(searchText: String, page: Int) -> URL? {
        let tunnel = "https://"
        let server = "serpapi.com"
        let method = "/search.json"
        let params = "?q=\(searchText)&tbm=isch&num=20&device=mobile&ijn=\(page)&api_key=\(apiKey)"
        let string = tunnel + server + method + params
        let url = URL(string: string)
        
        return url
    }
    
    func getImages(searchText: String, page: Int) async throws -> SearchData {
        guard let url = self.createURL(searchText: searchText,
                                       page: page) else {
            
            throw HTTPError.badURL
        }
        
        print(url)
        
        guard let response = try? await URLSession.shared.data(from: url) else {
            print("Bad Request")
            throw HTTPError.badRequest
        }
        
        let data = response.0
//        
//        let json = JSON(data)
//        print(json)
//        
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        guard let rezultData = try? jsonDecoder.decode(SearchData.self,
                                                       from: data) else {
            print("Bad Response")
            throw HTTPError.badResponse
        }
        
        return rezultData
    }

    func downloadImage(by url: String) async throws -> Data? {
        guard let url = URL(string: url) else { return nil }
        let session = URLSession.shared
        let data = try await session.data(from: url).0
        return data
    }
}


enum HTTPError: Error {
    case badURL, badRequest, badResponse, badGetaway
}
